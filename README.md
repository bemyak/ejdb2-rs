# EJDB2-RS

Rust bindings for the [EJDB](https://ejdb.org/) database. Think of SQLite, but document-oriented.

The project is its early stages, only generation of the `-sys` crate is done. Contibutions are welcome!
