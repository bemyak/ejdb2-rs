use ejdb2_rs_sys::{EJDB, EJDB_OPTS};
use std::ffi::CString;

pub struct Database {
    pub(crate) inner: EJDB,
    #[allow(dead_code)]
    pub(crate) ejdb_opts: EJDB_OPTS,
    #[allow(dead_code)]
    pub(crate) strings: Vec<CString>,
}

impl Drop for Database {
    fn drop(&mut self) {
        println!("Closing EJDB");
        unsafe {
            ejdb2_rs_sys::ejdb_close(&mut self.inner);
        }
    }
}
