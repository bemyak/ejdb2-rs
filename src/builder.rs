use crate::database::Database;
use crate::DatabaseOpenMode;
use ejdb2_rs_sys::{EJDB, EJDB_OPTS};
use rand::RngCore;
use std::error;
use std::ffi::CString;
use std::fmt;
use std::ptr;
use std::result::Result;

pub struct EJDB2Builder {
    ejdb_opts: EJDB_OPTS,
    strings: Vec<CString>,
}

#[derive(Debug)]
pub enum EjdbError {
    InitError,
    OpenError(String),
}

impl fmt::Display for EjdbError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            EjdbError::InitError => write!(f, "Failed to initialize database!"),
            EjdbError::OpenError(file) => write!(f, "Cannot open databse: {}", file),
        }
    }
}

impl error::Error for EjdbError {}

impl EJDB2Builder {
    pub fn new<P: Into<Vec<u8>>>(path: P) -> EJDB2Builder {
        let mut rng = rand::thread_rng();
        let db_path = CString::new(path).expect("invalid path specified");
        let db_path_ptr = db_path.as_ptr();
        let mut res = EJDB2Builder {
            strings: vec![db_path],
            ejdb_opts: EJDB_OPTS::default(),
        };
        res.ejdb_opts.kv.path = db_path_ptr;
        res.ejdb_opts.kv.random_seed = rng.next_u32();
        res
    }
}

impl EJDB2Builder {
    pub fn build(self) -> Result<Database, EjdbError> {
        let mut ejdb_ptr: EJDB = ptr::null_mut();
        let err = unsafe { ejdb2_rs_sys::ejdb_init() };
        if err != 0 {
            return Err(EjdbError::InitError);
        }
        println!("Running EJDB with options: {:#?}", &self.ejdb_opts);
        let err = unsafe { ejdb2_rs_sys::ejdb_open(&self.ejdb_opts, &mut ejdb_ptr) };
        if err != 0 {
            return Err(EjdbError::OpenError(
                self.strings[0].to_str().unwrap().to_owned(),
            ));
        }
        Ok(Database {
            inner: ejdb_ptr,
            ejdb_opts: self.ejdb_opts,
            strings: self.strings,
        })
    }
    pub fn oflags(mut self, oflags: DatabaseOpenMode) -> EJDB2Builder {
        self.ejdb_opts.kv.oflags = oflags.bits();
        self
    }
    pub fn file_lock_fail_fast(mut self, file_lock_fail_fast: bool) -> EJDB2Builder {
        self.ejdb_opts.kv.file_lock_fail_fast = file_lock_fail_fast;
        self
    }
    pub fn wal(mut self, wal: bool) -> EJDB2Builder {
        self.ejdb_opts.no_wal = !wal;
        self
    }
    pub fn sort_buffer_sz(mut self, sort_buffer_sz: u32) -> EJDB2Builder {
        self.ejdb_opts.sort_buffer_sz = sort_buffer_sz;
        self
    }
    pub fn document_buffer_sz(mut self, document_buffer_sz: u32) -> EJDB2Builder {
        self.ejdb_opts.document_buffer_sz = document_buffer_sz;
        self
    }
    pub fn enable_http<P: Into<Vec<u8>>>(
        mut self,
        port: u16,
        host: Option<P>,
        read_anon: bool,
    ) -> EJDB2Builder {
        self.ejdb_opts.http.enabled = true;
        self.ejdb_opts.http.port = port as i32;
        if let Some(host) = host {
            let host_c = CString::new(host).expect("invalid host specified");
            let host_c_ptr = host_c.as_ptr();
            self.strings.push(host_c);
            self.ejdb_opts.http.bind = host_c_ptr;
        }
        self.ejdb_opts.http.read_anon = read_anon;
        self.ejdb_opts.http.blocking = false;
        self
    }
}
