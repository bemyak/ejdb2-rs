#[cfg(test)]
mod test {
    use crate::builder::EJDB2Builder;
    use crate::*;
    use std::env;
    use std::fs;
    use std::net;
    use std::ptr;
    use std::thread;

    #[test]
    fn test_ejdb_version_full() {
        assert!(ejdb_version_full() == "2.0.46");
        cleanup();
    }

    #[test]
    fn test_ejdb_open() {
        let opts = EJDB2Builder::new(get_tmp_path());
        let db = opts.build().unwrap();
        assert_ne!(db.inner, ptr::null_mut());
        cleanup();
    }

    #[test]
    fn test_ejdb_open2() {
        let opts = EJDB2Builder::new(get_tmp_path());
        let db = opts.build().unwrap();
        assert_ne!(db.inner, ptr::null_mut());
        cleanup();
    }

    #[test]
    fn test_ejdb_http() {
        let http_port = 2222;
        let _db = EJDB2Builder::new(get_tmp_path())
            .wal(false)
            .enable_http::<&str>(http_port, None, true)
            .oflags(DatabaseOpenMode::IWKV_TRUNC)
            .build()
            .unwrap();
        std::thread::sleep(std::time::Duration::from_secs(1));
        println!("Checking port...");
        // TODO: Not working currently, needs investigation
        // assert!(!port_is_available(http_port));
        cleanup();
    }

    fn port_is_available(port: u16) -> bool {
        match net::TcpListener::bind(("127.0.0.1", port)) {
            Ok(_) => true,
            Err(_) => false,
        }
    }

    fn get_tmp_path() -> String {
        let thread_id = format!("{:?}", thread::current().id());
        let file_name: String = "ejdb_test_".to_owned() + thread_id.as_ref();
        let result = env::temp_dir().join(file_name).to_str().unwrap().to_owned();
        println!("{}", result);
        result
    }

    fn cleanup() {
        let _ = fs::remove_file(get_tmp_path());
        let _ = fs::remove_file(get_tmp_path() + "-wal");
    }
}
