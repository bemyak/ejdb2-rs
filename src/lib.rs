extern crate ejdb2_rs_sys;
extern crate rand;
#[macro_use]
extern crate bitflags;

pub mod builder;
pub mod database;
pub use ejdb2_rs_sys::*;
mod test;

use std::ffi::CStr;
use std::str;

bitflags! {
    pub struct DatabaseOpenMode: u8 {
        /** Open storage file in read-only mode */
        const IWKV_RDONLY                  = 0x2;
        /** Truncate storage file on open */
        const IWKV_TRUNC                   = 0x4;
    }
}

pub fn ejdb_version_full() -> &'static str {
    let version_ptr = unsafe { ejdb2_rs_sys::ejdb_version_full() };
    let c_str = unsafe { CStr::from_ptr(version_ptr) };
    c_str.to_str().unwrap()
}
